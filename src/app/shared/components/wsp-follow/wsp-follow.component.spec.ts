import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WspFollowComponent } from './wsp-follow.component';

describe('WspFollowComponent', () => {
  let component: WspFollowComponent;
  let fixture: ComponentFixture<WspFollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WspFollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WspFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
