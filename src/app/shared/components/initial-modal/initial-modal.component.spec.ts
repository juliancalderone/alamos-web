import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialModalComponent } from './initial-modal.component';

describe('InitialModalComponent', () => {
  let component: InitialModalComponent;
  let fixture: ComponentFixture<InitialModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
