import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class GoogleSheetsService {
  // TODO: move to environment.ts file
  private readonly SHEET_ID = "1LNMCNIbIOPCmqYL_42qWcKXmcutMeVGi-08DMbIM7NQ";
  private readonly API_KEY = "AIzaSyCsEtNRFcubN4IqThY5uoOTMLKTl8Wi1vU";
  private readonly SHEET_RANGE = "porcentajes!A1:C25";

  constructor(private httpClient: HttpClient) {}

  // obtain data from Google Sheets API y almacenarla en un archivo JSON
  getGoogleSheetData(): Observable<any> {
    const url = `https://sheets.googleapis.com/v4/spreadsheets/${this.SHEET_ID}/values/${this.SHEET_RANGE}?key=${this.API_KEY}`;
    return this.httpClient.get(url).pipe(
      catchError((error) => {
        console.error("Error reading Google Sheet", error);
        return [];
      })
    );
  }
}
