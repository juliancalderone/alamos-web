import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public news = [
    {
      description: 'Estamos presentes en la sección ARQ del diario Clarín.',
      logo: 'assets/clarin.svg',
      url: 'https://www.kiosco.clarin.com/arq/20221206/page/25',
      img: 'assets/nota-clarin.png',
      ctaLabel: 'Ver nota'
    },
    {
      description: 'Presentes en las salas de espera de los centros de Diagnostico Dim y en la  Revista Digital de Salud y Bienestar.',
      logo: 'assets/dim.svg',
      url: 'https://heyzine.com/flip-book/b3d2761471.html#page/16',
      img: 'assets/nota-dim.png',
      ctaLabel: 'Visitar página'
    },
    {
      description: 'Presentes en las sedes de Sport Club, para sumar beneficios.',
      logo: 'assets/sport-club.svg',
      url: 'https://wa.link/98h9nh',
      img: 'assets/nota-3.png',
      ctaLabel: 'Consultar'
    },
  ]

}
