import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AboutUsRoutingModule } from "./about-us-routing.module";
import { AboutUsMainComponent } from './components/about-us-main/about-us-main.component';

@NgModule({
  imports: [CommonModule, AboutUsRoutingModule],
  declarations: [AboutUsMainComponent],
})
export class AboutUsModule {}
