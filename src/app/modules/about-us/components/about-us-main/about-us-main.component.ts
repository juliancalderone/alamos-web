import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";

export interface TeamMember {
  name: string;
  position: string;
  image: string;
  linkedin?: string;
}

@Component({
  selector: "app-about-us-main",
  templateUrl: "./about-us-main.component.html",
  styleUrls: ["./about-us-main.component.scss"],
})
export class AboutUsMainComponent implements OnInit {
  @ViewChild("tallyContainer", { read: ElementRef })
  tallyContainer: ElementRef;

  isTallyLoaded = false;

  constructor(private renderer: Renderer2) {}

  teamMembers: TeamMember[] = [
    {
      name: "Sebastián Gregorace",
      position: "CEO",
      image: "assets/team/seba.png",
      linkedin: "https://www.linkedin.com/in/sebastiangregorace",
    },
    {
      name: "Leandro Rodriguez Reyna",
      position: "Sales Manager",
      image: "assets/team/lea.png",
      linkedin: "https://www.linkedin.com/in/leandrorodriguezreina/",
    },
    {
      name: "Paula Balotta",
      position: "Mkt Manager",
      image: "assets/team/pau.png",
      linkedin: "https://www.linkedin.com/in/paulabalotta/",
    },
    {
      name: "Karina Tkaczuk",
      position: "Adm. & Accounting",
      image: "assets/team/kari.png",
      linkedin: "https://www.linkedin.com/in/karina-tkaczuk/",
    },
  ];

  ngOnInit() {
    this.loadTallyForm();
  }

  loadTallyForm() {
    const script = this.renderer.createElement("script");
    script.src = "https://tally.so/widgets/embed.js";
    this.renderer.appendChild(document.head, script);

    script.onload = () => {
      this.isTallyLoaded = true;
      // @ts-ignore - Tally is loaded globally
      window.Tally.loadEmbeds();
    };
  }
}
