import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AboutUsMainComponent } from "./components/about-us-main/about-us-main.component";

const routes: Routes = [
  {
    path: "",
    component: AboutUsMainComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutUsRoutingModule {}
