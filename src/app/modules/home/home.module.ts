import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwiperModule } from "ngx-swiper-wrapper";
import { HomeRoutingModule } from './home-routing.module';
import { LandingHomeComponent } from './components/landing/landing.component';
import { MainComponent } from './components/main/main.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { TeamComponent } from './components/team/team.component';
import { DevsComponent } from './components/devs/devs.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';

import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EmailService } from 'src/app/core/services/email.service';
import { ScrollToModule } from "ng2-scroll-to-el";
import { ParallaxComponent } from './components/parallax/parallax.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReviewsComponent } from './components/reviews/reviews.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SwiperModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    SharedModule
  ],
  declarations: [
    LandingHomeComponent,
    MainComponent,
    AboutUsComponent,
    TeamComponent,
    DevsComponent,
    ContactUsComponent,
    ParallaxComponent,
    ReviewsComponent,
  ],
  providers: [EmailService],
})
export class HomeModule {}
