import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { InvestorsComponent } from "../investors/components/investors/investors.component";
import { LandingHomeComponent } from "./components/landing/landing.component";

const routes: Routes = [
  {
    path: '',
    component: LandingHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
