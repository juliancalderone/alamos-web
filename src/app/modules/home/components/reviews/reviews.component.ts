import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  reviews = [
    {
      user: 'Metro Home Design',
      comment: `
        Un placer trabajar con ALAMOS y con todo el equipo que le ponen un empuje increíble para que cada proyecto
        salga mejor de lo planificado! Sin dudas el agregado de valor, y el grupo humano es lo que los hace diferentes!!
      `,
      initial: 'MHD',
      date: 'hace 24 días'
    },
    {
      user: 'Magalí Jacobián',
      comment: `
        Excelente atención y acompañamiento de parte de todos,
        cumplieron mi sueño de tener mi dpto propio🙌🏽…
      `,
      initial: 'MJ',
      date: 'hace 2 meses'
    },
    {
      user: 'Claudio Usandivares',
      comment: `
        Profesionales, confiables, y siempre con buena predisposición. Gracias!!!
      `,
      initial: 'CU',
      date: 'hace 4 meses'
    },
    {
      user: 'Fernando Perfetti',
      comment: `
        Una excelente opción para realizar tus proyectos de crecimiento. Buena gente.
        Muy recomendables. Serios y responsables.
      `,
      initial: 'FP',
      date: 'hace 6 meses'
    },
    {
      user: 'Fernando Castro',
      comment: `
        Desarrolladora muy recomendable y de confianza, muy prolijos para presentar el proyecto
        y para informar a los inversores el avance de obra. Están los dueños supervisando la obra
        todo el tiempo. Tienen un gran futuro!!!
      `,
      initial: 'FC',
      date: 'hace 6 meses'
    },
    {
      user: 'Alan R.',
      comment: `
        Super recomendable. Un placer poder invertir con gente que tiene el nivel de atención de la gente de Álamos.
        Se manejan para que entiendas todo y sepas el paso a paso de cada proyecto.
      `,
      initial: 'AR',
      date: 'hace un año'
    },
    {
      user: 'Nicolás Barbesini',
      comment: `
        Excelente servicio, siempre atentos y manteniendo informado al cliente en todo momento!
      `,
      initial: 'NB',
      date: 'hace un año'
    },
    {
      user: 'Analia Menendez',
      comment: `
        Atentos a cualquier consulta, dando una respuesta rápida y resolutiva
      `,
      initial: 'AM',
      date: 'hace dos años',
    },
    {
      user: 'Analía G. Szpur',
      comment: `
        Todo muy bien. Muy profesionales
      `,
      initial: 'AS',
      date: 'hace dos años',
    },
    {
      user: 'Ignacio Deus',
      comment: `
        Atención y profesionalidad de un equipo joven y dinámico.
      `,
      initial: 'ID',
      date: 'hace dos años',
    },
  ]

}
