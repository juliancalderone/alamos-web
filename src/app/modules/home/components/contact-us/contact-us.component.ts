import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { EmailService } from "../../../../core/services/email.service";

declare var gsap;

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.scss"],
})
export class ContactUsComponent implements OnInit {
  public isMobile: boolean;
  isTallyLoaded = false;
  @ViewChild("tallyContactForm", { read: ElementRef })
  tallyContactForm: ElementRef;
  @ViewChild("contactFormElement")
  contactFormElement: ElementRef<HTMLDivElement>;
  @ViewChild("contactInfo") contactInfo: ElementRef<HTMLDivElement>;

  constructor(private renderer: Renderer2) {
    this.isMobile = this.detectMobileDevice();
  }

  ngOnInit() {
    this.sectionScrollAnimations();
    this.loadTallyForm();
  }

  loadTallyForm() {
    const script = this.renderer.createElement("script");
    script.src = "https://tally.so/widgets/embed.js";
    this.renderer.appendChild(document.head, script);
    script.onload = () => {
      this.isTallyLoaded = true;
      // @ts-ignore - Tally is loaded globally

      window.Tally.loadEmbeds();
    };
  }

  sectionScrollAnimations(): void {
    if (!this.isMobile) {
      gsap.from(this.contactFormElement.nativeElement, {
        scrollTrigger: {
          trigger: this.contactFormElement.nativeElement,
          scrub: false,
          start: "-400px center",
        },
        duration: 0.8,
        x: 1200,
        opacity: 1,
      });
      gsap.from(this.contactInfo.nativeElement, {
        scrollTrigger: {
          trigger: this.contactInfo.nativeElement,
          scrub: false,
          start: "-400px center",
        },
        duration: 0.8,
        x: -1200,
        opacity: 1,
      });
    }
  }

  detectMobileDevice(): boolean {
    const userAgent = window.navigator.userAgent.toLowerCase();
    const mobileKeywords = [
      "iphone",
      "android",
      "webos",
      "ipad",
      "ipod",
      "blackberry",
      "windows phone",
    ];

    return mobileKeywords.some((keyword) => userAgent.includes(keyword));
  }
}
