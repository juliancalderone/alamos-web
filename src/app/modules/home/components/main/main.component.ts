import { Component, ViewChild, AfterViewInit, ElementRef, OnInit } from "@angular/core";
import { isMobile } from "src/app/helpers/is-mobile";
declare var gsap;


@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
})
export class MainComponent implements OnInit {
  @ViewChild('buttons') buttons: ElementRef<HTMLDivElement>;
  @ViewChild("videoElement") videoElement: ElementRef;
  private video: HTMLMediaElement;

  constructor() { }

  playVideo() {
    this.video = this.videoElement.nativeElement;
    this.video.muted = true;
    this.video.loop = true;
    this.video.play();
  }

  ngAfterViewInit() {
    console.log(this.videoElement)
    this.playVideo();

  }


  ngOnInit() {
    this.sectionScrollAnimations();
  }

    sectionScrollAnimations(): void {

    gsap.to(this.buttons.nativeElement, {
      scrollTrigger: {
        trigger: this.buttons.nativeElement,
        scrub: true,
        start: '200px center',
      },
      duration: 0.4,
      y: 120,
      opacity: 0,
    });
  }
}
