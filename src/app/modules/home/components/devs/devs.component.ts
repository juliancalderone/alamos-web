import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-devs",
  templateUrl: "./devs.component.html",
  styleUrls: ["./devs.component.scss"],
})
export class DevsComponent implements OnInit {
  @ViewChild("devsRef") devsRef: ElementRef;

  devs = [
    {
      name: "MARE | HAEDO",
      address: "Lainez 1325",
      status: "Finalizado",
      spaces: "| 2 y 3 ambientes |",
      img: "assets/mare.jpg",
      slug: "mare",
    },
    {
      name: "TERRA | HAEDO",
      address: "Lainez 1346",
      status: "En pozo",
      spaces: "| 1, 2 y 3 ambientes |",
      img: "assets/terra.jpg",
      slug: "terra",
    },
    {
      name: "NUBI | HAEDO",
      address: "Av. Pte. Perón 2246",
      status: "En pozo",
      spaces: "| 2 y 3 ambientes |",
      img: "assets/nubi-haedo.jpg",
      slug: "nubi",
    },
    {
      name: "FUOCO | HAEDO",
      address: "Av. Pte. Perón 2730/6",
      status: "En preventa",
      spaces: "| 2 y 3 ambientes |",
      img: "assets/fuoco.png",
      slug: "fuoco",
    },
  ];

  constructor() {}

  ngOnInit() {}

  scrolltop() {
    window.scrollTo(0, 0);
  }
}
