import { Component, ElementRef, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  @ViewChild('about-us') aboutUs: ElementRef;

  constructor() { }

  ngOnInit() {
  }

}
