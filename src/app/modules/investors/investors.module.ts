import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestorsRoutingModule } from './investors-routing.module';
import { InvestorsComponent } from './components/investors/investors.component';

@NgModule({
  imports: [
    CommonModule,
    InvestorsRoutingModule
  ],
  declarations: [
    InvestorsComponent
  ]
})
export class InvestorsModule { }
