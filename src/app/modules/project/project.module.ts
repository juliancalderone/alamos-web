import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Swiper
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

import { ProjectRoutingModule } from './project-routing.module';
import { LandingProjectComponent } from './components/landing/landing.component';
import { SliderComponent } from './components/slider/slider.component';
import { DescComponent } from './components/desc/desc.component';
import { LocationComponent } from './components/location/location.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { InitialModalComponent } from 'src/app/shared/components/initial-modal/initial-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: "horizontal",
  slidesPerView: "auto",
};

@NgModule({
  imports: [
    CommonModule,
    ProjectRoutingModule,
    SwiperModule,
    ModalModule.forRoot(),
    SharedModule
  ],
  declarations: [
    LandingProjectComponent,
    SliderComponent,
    DescComponent,
    LocationComponent,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class ProjectModule {}
