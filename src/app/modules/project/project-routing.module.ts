import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingProjectComponent } from './components/landing/landing.component';

const routes: Routes = [
  {
    path: '',
    component: LandingProjectComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
