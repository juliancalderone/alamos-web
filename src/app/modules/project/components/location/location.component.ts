import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: "app-location",
  templateUrl: "./location.component.html",
  styleUrls: ["./location.component.scss"],
})
export class LocationComponent implements OnInit {
  @Input() project;

  lat = -4.1074655;
  lng = -81.0573487;

  // public zoom = 16;

  places: any[] = [
    {
      category: "Transporte",
      items: [
        {
          name: "Estación de tren HAEDO - Sarmiento ",
        },
        {
          name: "Metrobús",
        },
        {
          name: "Au Acceso Oeste",
        },
      ],
    },
    {
      category: "Educación",
      items: [
        {
          name: "Sagrada Familia",
        },
        {
          name: "Guillermo Almirante Brown",
        },
        {
          name: "Escuela Media Nº 2",
        },
        {
          name: "Instituto Advent - Osimato",
        },
        {
          name: "Universidad de Morón",
        },
        {
          name: "Universidad Tecnológica Nacional ",
        },
      ],
    },
    {
      category: "Salud",
      items: [
        {
          name: "Sanatorio La Trinidad Ramos Mejia (4 km)",
        },
        {
          name: "Hospital posadas (3 km)",
        },
        {
          name: "Hospital San Juan de Dios (4 km)",
        },
        {
          name: "Hospital Güemes (2 km)",
        },
      ],
    },
    {
      category: "Locales comerciales",
      items: [
        {
          name: "MC Donald",
        },
        {
          name: "Shopping Show Case",
        },
        {
          name: "Disco - Carrefour",
        },
        {
          name: "Correo argentino",
        },
        {
          name: "Pago fácil - Rapipago",
        },
      ],
    },
    // {
    //   category: 'Bancos',
    //   items: [
    //     {
    //       name: 'Santander Río',
    //     },
    //     {
    //       name: 'BBVA Francés',
    //     },
    //     {
    //       name: 'Patagonia',
    //     },
    //     {
    //       name: 'Credicoop',
    //     },
    //     {
    //       name: 'Galicia',
    //     },
    //   ],
    // },
  ];

  constructor() {}

  ngOnInit() {}
}
