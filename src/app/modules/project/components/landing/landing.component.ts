import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { GoogleSheetsService } from "src/app/core/services/google-sheets.service";
import { dataProjects } from "src/app/helpers/catalog";

@Component({
  selector: "app-landing-project",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"],
})
export class LandingProjectComponent implements OnInit {
  project: any;
  items: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private googleSheetsService: GoogleSheetsService
  ) {}

  private iconMap: { [key: string]: string } = {
    Demolición: "assets/icons/demolition.svg",
    Excavación: "assets/icons/excavation.svg",
    Estructuras: "assets/icons/joint.svg",
    Mampostería: "assets/icons/wall.svg",
    Instalaciones: "assets/icons/socket.svg",
    Terminaciones: "assets/icons/window.svg",
  };

  transformData(dataArray: any[][]): any[] {
    const result = [];

    for (let i = 1; i < dataArray.length; i++) {
      const [project, tag, percent] = dataArray[i];

      let projectEntry = result.find((entry) => entry.project === project);
      if (!projectEntry) {
        projectEntry = { project, items: [] };
        result.push(projectEntry);
      }

      projectEntry.items.push({
        tag,
        icon: this.iconMap[tag],
        percent,
      });
    }

    return result;
  }

  getTransformedPercentageData(): Observable<any[]> {
    return this.googleSheetsService.getGoogleSheetData().pipe(
      map((data: any) => {
        const sheetData = data.values;
        return this.transformData(sheetData);
      }),
      catchError((error) => {
        console.error("Error reading Google Sheet", error);
        return [];
      })
    );
  }

  getProjectBySlug() {
    const slug = this.route.snapshot.paramMap.get("slug");
    const project = dataProjects.find((item) => item.slugProject === slug);

    this.getTransformedPercentageData().subscribe((data) => {
      this.items = data;
      this.items.filter((item) => {
        if (item.project === project.slugProject) {
          project.items = item.items;
        }
      });
    });

    if (!project) {
      this.router.navigate(["/"]);
    } else {
      this.project = project;
    }
  }

  ngOnInit() {
    this.getProjectBySlug();
  }
}
