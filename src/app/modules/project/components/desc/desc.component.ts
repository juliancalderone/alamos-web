import { Component, Input, OnInit } from "@angular/core";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";

@Component({
  selector: "app-desc",
  templateUrl: "./desc.component.html",
  styleUrls: ["./desc.component.scss"],
})
export class DescComponent implements OnInit {
  @Input() project;

  swiperTop: SwiperConfigInterface = {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: true,
    loop: true,
    initialSlide: 0,
    effect: "fade",
    // fadeEffect: {
    //   crossFade: true
    // },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  swiperFooter: SwiperConfigInterface = {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: true,
    loop: true,
    initialSlide: 0,
    effect: "fade",
    // fadeEffect: {
    //   crossFade: true
    // },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  constructor() {}

  ngOnInit() {}
}
