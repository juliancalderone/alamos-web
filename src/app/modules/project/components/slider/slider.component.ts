import { Component, Input, OnInit } from "@angular/core";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";

@Component({
  selector: "app-slider",
  templateUrl: "./slider.component.html",
  styleUrls: ["./slider.component.scss"],
})
export class SliderComponent implements OnInit {
  @Input() project;

  swiperTop: SwiperConfigInterface = {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
      delay: 3500,
    },
    loop: true,
    initialSlide: 0,
    effect: "fade",
    // fadeEffect: {
    //   crossFade: true
    // },

    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  constructor() {}

  ngOnInit() {
    console.log(this.project);
  }
}
