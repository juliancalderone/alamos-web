export const dataProjects = [
  {
    slugProject: "mare",
    address: "Lainez 1325",
    slogan: "Viví rodeado <strong>de verde</strong>",
    environment: {
      textOne:
        "Ubicado en un barrio residencial que combina el encanto de las calles arboladas y las tradicionales casas.",
      textTwo:
        'A pocos metros de centros de transportes y acceso a la autopista. <span class="font-weight-medium">Un entorno ideal para familias,</span> con espacios verdes, escuelas y comercios de esparcimiento.',
    },
    name: "Mare",
    mainCover: "/assets/mare/mainCover.jpg",
    brochure:
      "https://drive.google.com/file/d/1B1pvgg9hXXW5O2BU0kEO-XlkJgdLOonU/view?usp=drive_link",
    features: {
      typologies: [
        "2 Ambientes - Semipisos 40m²",
        "3 Ambientes - Pisos 80m²",
        "Todos con balcón y parrilla propia",
      ],
      amenities: [
        "Terraza con solarium",
        "Duchas verticales",
        "Laundry",
        "Open sum",
        "Jacuzzi",
      ],
      characteristics: [
        "MARE combina elegancia y confort en sus unidades. Los departamentos son luminosos y ventilados, gracias a la estratégica orientación de los pisos y semipisos.",
        "Cuentan con amplios balcones y una sublime terraza, rodeada de vegetación y naturaleza.",
        "La planta baja cuenta con cocheras vehiculares, bicicleteros y espacios verdes.",
        "En el piso superior los habitantes podrán disfrutar de diversos amenities al aire libre con las hermosas vistas de Haedo",
      ],
    },
    progress: "Edificio finalizado",
    items: [],
    topSlides: [
      {
        img: "/assets/mare/cover-one.jpg",
      },
      {
        img: "/assets/mare/cover-two.jpg",
      },
      {
        img: "/assets/mare/cover-three.jpg",
      },
    ],
    footerSlides: [
      {
        img: "/assets/mare/mare-interior-1.jpg",
      },
      {
        img: "/assets/mare/mare-interior-3.jpg",
      },
      {
        img: "/assets/mare/mare-interior-2.jpg",
      },
      {
        img: "/assets/mare/mare-interior-4.jpg",
      },
      {
        img: "/assets/mare/mare-interior-5.jpg",
      },
      {
        img: "/assets/mare/mare-interior-6.jpg",
      },
    ],

    // coverOne: '/assets/mare/cover-one.jpg',
    coverTwo: "/assets/mare/cover-two.jpg",
    // coverThree: '/assets/mare/cover-three.jpg',
    location:
      "Un edificio en el corazón de Haedo, un barrio en pleno auge, donde confluyen una gran oferta de locales comerciales, cafeterías y restaurantes que se mezclan con la tranquilidad del barrio.  A muy pocas cuadras de la Au. Acceso Oeste, a 5 min de la estación Haedo y a sólo 100 mts. del Metrobus y parque lineal con accesos cómodos a toda la ciudad.",
  },
  {
    slugProject: "terra",
    slogan: "Viví rodeado de verde",
    environment: {
      textOne:
        'Queremos que <span class="font-weight-medium">disfrutes de la naturaleza</span> sin salir de tu casa:',
      items: [
        "Cómodas y luminosas unidades",
        "Amplios balcones vivos y terrazas",
        "Pasillos semi-cubiertos con vegetación vertical",
        "Espacios abiertos",
        "Jardines internos",
      ],
      textTwo:
        'Esperamos que <span class="font-weight-medium">en cada rincón puedas conectar con eso que te hace bien. </span> Una experiencia única para los que se animan a vivir distinto.',
    },
    address: "Lainez 1346",
    name: "Terra",
    brochure:
      "https://drive.google.com/file/d/1xqtKssdcKhovERw6kOgleiC_wron-roU/view?usp=drive_link",
    features: {
      typologies: ["1 ambiente", "2 ambientes", "3 ambientes"],
      amenities: [
        "Jacuzzi",
        "Solarium",
        "Cocheras",
        "Espacio Terra",
        "Parrillas individuales",
        "Bicicleteros",
      ],
      characteristics: [
        "TERRA ofrece cómodas y luminosas unidades de vivienda, con amplios balcones y terrazas, rodeados de verde y confort.",
        "El edificio contará con equipamiento de seguridad digital en los accesos, sistema de cámaras de vigilancia de espacios comunes y cercos perimetrales eléctricos.",
      ],
    },
    mainCover: "/assets/terra/mainCover.jpg",
    progress: "Avance de obra",
    items: [],
    topSlides: [
      {
        img: "assets/sliders/bg-01.jpg",
      },
      {
        img: "assets/sliders/bg-02.jpg",
      },
      {
        img: "assets/sliders/bg-03.jpg",
      },
      {
        img: "assets/sliders/bg-07.jpg",
      },
      {
        img: "assets/sliders/bg-08.jpg",
      },
    ],
    coverOne: "/assets/terra/cover-one.jpg",
    coverTwo: "/assets/terra/cover-two.jpg",
    coverThree: "/assets/terra/cover-three.jpg",
    location:
      "Un edificio en el corazón de Haedo, un barrio en pleno auge, donde confluyen una gran oferta de locales comerciales, cafeterías y restaurantes que se mezclan con la tranquilidad del barrio.\nA muy pocas cuadras de la Au. Acceso Oeste, a 5 min. de la estación Haedo y a sólo 100 mts. del Metrobus y parque lineal con accesos cómodos a toda la ciudad.",
  },
  {
    slugProject: "nubi",
    environment: {
      textOne:
        "Caracterizado por su imponente estructura moderna de hormigón, diseño minimalista y recursos sustentables para cuidar nuestro medio ambiente.",
      textTwo:
        "NUBI ofrece cómodas y luminosas unidades de vivienda, con amplios balcones y terrazas, rodeados de verde y confort.",
    },
    address: "Av. Pte. Perón 2246",
    name: "Nubi",
    brochure:
      "https://drive.google.com/file/d/1jlvumSHvTqoQrfKxR_9ScgBADx2bkBbE/view?usp=drive_link",
    features: {
      typologies: [
        "2 Ambientes - Semipisos 44m²",
        "3 Ambientes - Pisos 88m²",
        "Todos con balcón propio",
      ],
      amenities: [
        "Jacuzzi",
        "Terraza con solarium",
        "Duchas de exterior",
        "Cocheras",
        "Espacio NUBI",
        "WiFi en espacios comunes",
        "Sector de home office al aire libre",
      ],
      characteristics: [
        "El edificio estará equipado con sistemas de seguridad digital en los accesos, cámaras de vigilancia en espacios comunes y cercos perimetrales eléctricos.",
        "La planta baja contará con cocheras vehiculares y espacios verdes.",
        "En el piso superior los habitantes podrán disfrutar de diversos amenities al aire libre con las hermosas vistas de Haedo.",
      ],
    },
    mainCover: "/assets/nubi/mainCover.jpg",
    progress: "Avance de obra",
    items: [],
    topSlides: [
      {
        img: "assets/nubi/slider-nubi-01.jpg",
      },
      {
        img: "assets/nubi/slider-nubi-06.jpg",
      },
      {
        img: "assets/nubi/slider-nubi-02.jpg",
      },
      {
        img: "assets/nubi/slider-nubi-03.jpg",
      },
      {
        img: "assets/nubi/slider-nubi-04.jpg",
      },
      {
        img: "assets/nubi/slider-nubi-05.jpg",
      },
    ],
    coverOne: "/assets/terra/cover-one.jpg",
    coverTwo: "/assets/nubi/reception.jpg",
    coverThree: "/assets/terra/cover-three.jpg",
    location:
      "Un edificio en el corazón de Haedo, un barrio en pleno auge, donde confluyen una gran oferta de locales comerciales, cafeterías y restaurantes que se mezclan con la tranquilidad del barrio.\nA muy pocas cuadras de la Au. Acceso Oeste, a 5 min. de la estación Haedo y a sólo 100 mts. del Metrobus y parque lineal con accesos cómodos a toda la ciudad.",
  },
  {
    slugProject: "fuoco",
    environment: {
      textOne:
        "Imaginá vivir rodeado de una arquitectura contemporánea en hormigón visto, un diseño minimalista que maximiza el confort, y un compromiso con la sustentabilidad que respeta nuestro entorno.",
      textTwo:
        "FUOCO, en el corazón de Haedo, te invita a vivir en espacios amplios, luminosos y en conexión con la naturaleza, donde cada detalle está pensado para brindarte armonía y bienestar.",
    },
    address: "Av. Pte. Perón 2730/6",
    name: "Fuoco",
    brochure:
      "https://drive.google.com/file/d/1OvjtdBw9SG_1re9H6kxGYMbptRUl1R8f/view",
    features: {
      typologies: [
        "2 Ambientes - Semipisos 45m²",
        "3 Ambientes - Pisos 90m²",
        "Todos con balcón propio",
      ],
      amenities: [
        "Terraza para usos múltiples",
        "Jacuzzi",
        "Duchas de exterior",
        "Cocheras",
        "Espacio ZEN FUOCO",
        "WiFi en espacios comunes",
        "Sector de home office al aire libre",
      ],
      characteristics: [
        "El edificio tendrá 7 y 8 pisos con 30 unidades funcionales de 2 y 3 ambientes, distribuidas en dos torres. Los balcones contarán con un sistema de vegetación y riego automático, integrando naturaleza al diseño urbano. La seguridad incluirá acceso digital, cámaras en áreas comunes y cercos eléctricos perimetrales.",
        "En planta baja habrá cocheras, bicicleteros y espacios verdes. En la terraza, FUOCO ofrecerá exclusivos amenities: jacuzzi, solarium, duchas exteriores, WiFi para home office, y un Espacio ZEN para relajarse con vistas únicas de Haedo.",
      ],
    },
    mainCover: "/assets/fuoco/mainCover.png",
    progress: "Avance de obra",
    items: [],
    topSlides: [
      {
        img: "assets/fuoco/slider-fuoco-01.png",
      },
      {
        img: "assets/fuoco/slider-fuoco-02.png",
      },
      {
        img: "assets/fuoco/slider-fuoco-03.png",
      },
      {
        img: "assets/fuoco/slider-fuoco-04.png",
      },
      {
        img: "assets/fuoco/slider-fuoco-05.png",
      },
      {
        img: "assets/fuoco/slider-fuoco-06.png",
      },
    ],
    coverOne: "/assets/terra/cover-one.jpg",
    coverTwo: "/assets/fuoco/zen-space.png",
    coverThree: "/assets/terra/cover-three.jpg",
    location:
      "Ubicado en un barrio vibrante y en pleno auge, FUOCO te conecta con la mejor oferta de locales comerciales, cafeterías y restaurantes, mientras te ofrece la tranquilidad de un entorno residencial. \nA solo pocas cuadras de la AU Acceso Oeste y a 5 minutos de la estación de Haedo, FUOCO está estratégicamente situado sobre el corredor del Metrobus y el Parque Lineal, un espacio verde pensado para la recreación y el esparcimiento.",
  },
];
