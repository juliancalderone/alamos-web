/* eslint-disable */
var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();

var config = {
    user: "alamos", // NOTE that this was username in 1.x
    password: "VC2pCvg9C", // optional, prompted if none given
    host: "ftp.alamos.com.ar",
    port: 21,
    localRoot: __dirname + '/dist',
    remoteRoot: '/public_html',
    include: ['*', '**/*'], // this would upload everything except dot files
    // include: ['index.html'],
    exclude: ['**/*.map', 'PHPMailer-master/**/*'], // e.g. exclude sourcemaps
    // deleteRemote: true              // delete existing files at destination before uploading
}

// use with promises
ftpDeploy.deploy(config)
  .then(res => console.log('finished'))
  .catch(err => console.log(err))

ftpDeploy.on('uploading', function (data) {
  const percentage = (data.transferredFileCount / data.totalFilesCount) * 100
  process.stdout.write(`[uploading] ${percentage.toFixed(2)}% [${data.filename}]`)
});

ftpDeploy.on('uploaded', function (data) {
  process.stdout.cursorTo(0);
  process.stdout.clearLine();
  // console.log("\x1b[32m", "[uploaded]", "\x1b[0m"); // same data as uploading event
});

ftpDeploy.on('upload-error', function (data) {
  console.log(data.err); // data will also include filename, relativePath, and other goodies
});
